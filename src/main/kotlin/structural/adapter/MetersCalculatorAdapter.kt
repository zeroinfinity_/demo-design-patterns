package structural.adapter

class MetersCalculatorAdapter(
    private val metersCalculator: MetersCalculator
) : KilometersCalculator(metersCalculator.car) {

    override fun computeKilometersPerHour(): Double {
        val MpS = metersCalculator.computeMetersPerSecond()

        val KmpH = (MpS * 18/5)
        println("Adapting $MpS MPS --> $KmpH KPH")
        return KmpH
    }
}