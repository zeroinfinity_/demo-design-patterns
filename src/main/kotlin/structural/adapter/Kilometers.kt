package structural.adapter

interface Kilometers {
    fun getKilometersPerHour(algo: Int): Float
}