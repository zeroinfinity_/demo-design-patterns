package structural.adapter

fun main(args: Array<String>) {
    val car = Car(0.05, 0.1)

    val processor = Processor(0.4)
    val calculator = KilometersCalculator(car)

    val metersCalculatorAdapter = MetersCalculatorAdapter(
        MetersCalculator(car)
    )

    processor.verify(calculator)
}

// Se encarga de procesar calculos para verificar
class Processor(
    private val minSpeed: Double
) {

    fun verify(kilometersCalculator: KilometersCalculator) {
        if (kilometersCalculator.computeKilometersPerHour() >= minSpeed) {
            println("Test speed average... passed")
        } else {
            println("Test speed average... NOT passed")
        }
    }
}

class Car(val distance: Double, val time: Double)