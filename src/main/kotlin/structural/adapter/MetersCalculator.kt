package structural.adapter

class MetersCalculator(
    val car: Car
) {

    fun computeMetersPerSecond(): Double {
        print("Calculando m/s...")
        val mph = (car.distance * 1000) / (car.time * 60 * 60)
        println(" ...$mph MPS")
        return mph
    }
}