package structural.adapter

// Se encarga de hacer cálculos sobre el carrito
open class KilometersCalculator(
    val car: Car
) {

    open fun computeKilometersPerHour(): Double {
        print("Calculando Km/h...")
        val kpH = car.distance/car.time
        println(" ...$kpH KPH")
        return kpH
    }
}