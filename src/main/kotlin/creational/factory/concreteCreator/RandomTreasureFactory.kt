package creational.factory.concreteCreator

import creational.factory.Treasure
import creational.factory.TreasureFactory
import creational.factory.concreteProduct.CommonTreasure
import creational.factory.concreteProduct.RoyalTreasure
import creational.factory.concreteProduct.SilverTreasure
import kotlin.random.Random

class RandomTreasureFactory : TreasureFactory() {
    override fun createTreasure(): Treasure {
        // Create and return a treasure randomly
        val random = Random.nextInt(0, 100)
        return when (random) {
            in 70..100 -> RoyalTreasure()
            in 20..69 -> SilverTreasure()
            else -> CommonTreasure()
        }
    }
}