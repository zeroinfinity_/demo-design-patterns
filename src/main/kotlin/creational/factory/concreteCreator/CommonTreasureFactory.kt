package creational.factory.concreteCreator

import creational.factory.Treasure
import creational.factory.TreasureFactory
import creational.factory.concreteProduct.CommonTreasure

class CommonTreasureFactory : TreasureFactory() {
    override fun createTreasure(): Treasure {
        // Create and return common treasure
        return CommonTreasure()
    }
}