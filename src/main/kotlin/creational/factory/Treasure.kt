package creational.factory

interface Treasure {
    fun dropItems()
}