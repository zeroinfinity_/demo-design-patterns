package creational.factory

import creational.factory.concreteCreator.CommonTreasureFactory
import creational.factory.concreteProduct.SilverTreasure

fun main(args: Array<String>) {

    val battleCalculator = BattleCalculator(
        CommonTreasureFactory()
    )

    for (i in 1..5) {
        battleCalculator._gameLogic()
    }
}

class BattleCalculator(
    private val treasureFactory: TreasureFactory
) {

    fun gameLogic() {
        // ...
        if (isEnemyDestroyed()) {
            val treasure = SilverTreasure()
            treasure.dropItems()
        }
        // ..
    }

    fun _gameLogic() {
        // ...
        if (isEnemyDestroyed()) {
            val treasure = treasureFactory.createTreasure()
            treasure.dropItems()
        }
        // ..
    }

    private fun isEnemyDestroyed(): Boolean {
        return true
    }

}
