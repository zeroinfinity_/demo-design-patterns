package creational.factory

abstract class TreasureFactory {
    abstract fun createTreasure() : Treasure
}