package creational.factory.concreteProduct

import creational.factory.Treasure

class SilverTreasure : Treasure {
    override fun dropItems() {
        println("Dropping silver items...")
    }
}