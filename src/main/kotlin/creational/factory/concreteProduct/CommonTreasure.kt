package creational.factory.concreteProduct

import creational.factory.Treasure

class CommonTreasure : Treasure {
    override fun dropItems() {
        println("Dropping common items...")
    }
}