package creational.factory.concreteProduct

import creational.factory.Treasure

class RoyalTreasure : Treasure {
    override fun dropItems() {
        println("Dropping royal items...")
    }
}