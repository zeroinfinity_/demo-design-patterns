package creational.builder

fun main(args: Array<String>) {

    //val contact = Contact("Kevin", "Meza", null, "981500649", null, "male", null, null, null)

    val contact = Contact.Builder("Kevin")
        .withLastName("Meza")
        .withPhone("987456888")
        .withGender("male")
        .build()

    val contact2 = Contact.Builder("Robinson")
        .withPhone("987456888")
        .build()

    val contact3 = Contact.Builder("Kevin")
        .withLastName("Meza")
        .withPhone("987456888")
        .withGender("male")
        .withEmail("meza.hin..@gmail.com")
        .withBirthday("July 24")
        .build()
    println(contact)

    // Alternative using Named Arguments
    val person = Person(
        firstName = "Kevin",
        lastName = "Meza"
    )

    // Alternative using Named Constructors
    val malePerson = Person.createMalePerson("Bruce", "Wayne", 30)
    val femalePerson = Person.createFemalePerson("Diana", "Prince", 800)
    println(malePerson)
    println(femalePerson)
}
