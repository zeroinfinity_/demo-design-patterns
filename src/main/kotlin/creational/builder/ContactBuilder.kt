package creational.builder

class ContactBuilder(
    private val firstName: String
) {
    private var lastName: String? = null
    private var age: Int? = null
    private var phone: String? = null
    private var address: String? = null
    private var gender: String? = null
    private var nickName: String? = null
    private var email: String? = null
    private var birthday: String? = null

    fun withLastName(lastName: String): ContactBuilder {
        this.lastName = lastName
        return this
    }
    fun withAge(age: Int): ContactBuilder {
        this.age = age
        return this
    }
    fun withPhone(phone: String): ContactBuilder {
        this.phone = phone
        return this
    }
    fun withAddress(address: String): ContactBuilder {
        this.address = address
        return this
    }
    fun withGender(gender: String): ContactBuilder {
        this.gender = gender
        return this
    }
    fun withNickName(nickName: String): ContactBuilder {
        this.nickName = nickName
        return this
    }
    fun withEmail(email: String): ContactBuilder {
        this.email = email
        return this
    }
    fun withBirthday(birthday: String): ContactBuilder {
        this.birthday = birthday
        return this
    }

    /*fun build(): Contact {
        val contact = Contact(firstName, lastName, age, phone, address, gender, nickName, email, birthday)
        return contact
    }*/
}