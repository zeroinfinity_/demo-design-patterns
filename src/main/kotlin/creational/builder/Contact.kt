package creational.builder

data class Contact private constructor(
    private val firstName: String,
    private val lastName: String?,
    private val age: Int?,
    private val phone: String?,
    private val address: String?,
    private val gender: String?,
    private val nickName: String?,
    private val email: String?,
    private val birthday: String?
) {

    class Builder(
        private val firstName: String
    ) {
        private var lastName: String? = null
        private var age: Int? = null
        private var phone: String? = null
        private var address: String? = null
        private var gender: String? = null
        private var nickName: String? = null
        private var email: String? = null
        private var birthday: String? = null

        fun withLastName(lastName: String): Builder {
            this.lastName = lastName
            return this
        }
        fun withAge(age: Int): Builder {
            this.age = age
            return this
        }
        fun withPhone(phone: String): Builder {
            this.phone = phone
            return this
        }
        fun withAddress(address: String): Builder {
            this.address = address
            return this
        }
        fun withGender(gender: String): Builder {
            this.gender = gender
            return this
        }
        fun withNickName(nickName: String): Builder {
            this.nickName = nickName
            return this
        }
        fun withEmail(email: String): Builder {
            this.email = email
            return this
        }
        fun withBirthday(birthday: String) = apply {
            this.birthday = birthday
        }

        fun build(): Contact {
            val contact = Contact(firstName, lastName, age, phone, address, gender, nickName, email, birthday)
            return contact
        }
    }
}

data class Person(
    val firstName: String,
    val lastName: String? = null,
    val age: Int? = null,
    val gender: String? = null,
)
{
    companion object {
        fun createMalePerson(firstName: String, lastName: String, age: Int): Person {
            return Person(firstName, lastName, age, "Male")
        }

        fun createFemalePerson(firstName: String, lastName: String, age: Int): Person {
            return Person(firstName, lastName, age, "Female")
        }
    }
}