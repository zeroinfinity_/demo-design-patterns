package behavioral.observer

class TwitchChannel(
    val name: String
) {
    var uploadedVideo: String = ""

    fun uploadVideo(video: String) {
        println("Uploading new video...")
        uploadedVideo = video
    }
}