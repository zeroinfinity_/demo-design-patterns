package behavioral.observer

fun main(args: Array<String>) {
    val twitchChannel = TwitchChannel("K-Code")

    twitchChannel.uploadVideo("")
}