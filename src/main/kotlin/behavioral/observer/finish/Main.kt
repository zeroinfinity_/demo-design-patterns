package behavioral.observer.finish

fun main(args: Array<String>) {
    val twitchChannel = TwitchChannel("K-Code")

    val s1 = TwitchSubscriber("Kevin", twitchChannel)
    val s2 = TwitchSubscriber("Ronald", twitchChannel)
    val s3 = TwitchSubscriber("Josh", twitchChannel)

    twitchChannel.subscribe(s1)
    twitchChannel.subscribe(s2)
    twitchChannel.subscribe(s3)

    twitchChannel.uploadVideo("¡Ejemplo de Patrones de Diseño!")
}