package behavioral.observer.finish

class TwitchChannel(
    val name: String
) {
    val channelSubscribers = mutableListOf<Subscriber>()
    var uploadedVideo: String = ""

    fun uploadVideo(video: String) {
        println("Uploading new video...")
        uploadedVideo = video
        notifySubscribers()
    }

    fun subscribe(subscriber: Subscriber) {
        channelSubscribers.add(subscriber)
    }

    fun notifySubscribers() {
        channelSubscribers.forEach { subscriber ->
            subscriber.update()
        }
    }
}