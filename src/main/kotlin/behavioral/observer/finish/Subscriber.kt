package behavioral.observer.finish

interface Subscriber {
    fun update()
}