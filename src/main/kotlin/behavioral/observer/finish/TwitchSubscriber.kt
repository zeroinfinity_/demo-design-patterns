package behavioral.observer.finish

class TwitchSubscriber(
    private val name: String,
    private val twitchChannel: TwitchChannel
) : Subscriber {

    override fun update() {
        println("$name's Device:")
        println("${twitchChannel.name} acaba de subir un nuevo video: ${twitchChannel.uploadedVideo}")
    }
}