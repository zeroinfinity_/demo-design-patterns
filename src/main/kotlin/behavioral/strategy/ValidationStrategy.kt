package behavioral.strategy

interface ValidationStrategy {
    fun validateUser(user: User)
}