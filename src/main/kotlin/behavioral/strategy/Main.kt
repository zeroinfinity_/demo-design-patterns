package behavioral.strategy

import behavioral.strategy.`concrete-strategies`.FingerprintStrategy
import behavioral.strategy.`concrete-strategies`.PINStrategy
import behavioral.strategy.`concrete-strategies`.PasswordStrategy
import creational.factory.concreteCreator.RandomTreasureFactory
import creational.factory.concreteProduct.SilverTreasure

fun main(args: Array<String>) {
    // Se obtiene los datos del usuario
    val user = User("Kevin")
    var validationType = ValidationType.PASSWORD
    var validationStrategy = getValidation(validationType)


    val debtsSection = DebtsSectionContext(validationStrategy)
    debtsSection.validateToOpen(user)

    // El usuario cambia el tipo de validación
    validationType = ValidationType.FIGERPRINT
    println("\nCambiando configuración de validación...\n")

    validationStrategy = getValidation(validationType)
    debtsSection.setValidationStrategy(validationStrategy)
    debtsSection.validateToOpen(user)


    // ...
    when (validationType) {
        ValidationType.PASSWORD -> {
            // crear pantalla
            // mostrrar pantalla con cuadro de texto
            // validar password
            // abrir sección
        }
        ValidationType.PIN -> {
            // crear pantalla
            // mostrrar pantalla con 4 inputs
            // validar password
            // abrir sección
        }
        ValidationType.FIGERPRINT -> {
            // crear diálogo
            // mostrrar diálogo solicitando huella
            // validar huella con el dispositivo
            // abrir sección
        }
    }
    // ...
}

fun getValidation(validationType: ValidationType): ValidationStrategy {
    return when (validationType) {
        ValidationType.PASSWORD -> PasswordStrategy()
        ValidationType.PIN -> PINStrategy()
        ValidationType.FIGERPRINT -> FingerprintStrategy()
    }
}

data class User(
    val name: String
)

enum class ValidationType {
    PASSWORD,
    PIN,
    FIGERPRINT
}
