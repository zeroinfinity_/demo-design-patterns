package behavioral.strategy.`concrete-strategies`

import behavioral.strategy.User
import behavioral.strategy.ValidationStrategy

class PINStrategy : ValidationStrategy {
    override fun validateUser(user: User) {
        // Show a screen requesting the user PIN
        println("Mostrando pantalla de PIN...")
        println("<<Hola, ${user.name} ingrese su PIN de 4 dígitos>>")
    }
}