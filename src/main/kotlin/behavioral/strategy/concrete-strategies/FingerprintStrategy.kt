package behavioral.strategy.`concrete-strategies`

import behavioral.strategy.User
import behavioral.strategy.ValidationStrategy

class FingerprintStrategy : ValidationStrategy {
    override fun validateUser(user: User) {
        // Show a prompt requesting the user fingerprint
        println("Mostrando diálogo para huella...")
        println("<<Hola, ${user.name} confirme con su huella>>")
    }
}