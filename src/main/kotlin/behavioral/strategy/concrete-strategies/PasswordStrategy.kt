package behavioral.strategy.`concrete-strategies`

import behavioral.strategy.User
import behavioral.strategy.ValidationStrategy

class PasswordStrategy : ValidationStrategy {
    override fun validateUser(user: User) {
        // Show a screen requesting the user password
        println("Mostrando pantalla de contraseña...")
        println("<<Hola, ${user.name} ingrese su contraseña>>")
    }
}