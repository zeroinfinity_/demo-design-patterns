package behavioral.strategy

class DebtsSectionContext(
    private var validationStrategy: ValidationStrategy
) {

    // Es habitual que el contexto reciba la estrategia por constructor
    // pero también proporciona una función para modificar la estrategia
    fun setValidationStrategy(validationStrategy: ValidationStrategy) {
        this.validationStrategy = validationStrategy
    }

    // Este contexto delega parte del trabajo al objeto strategy de momento,
    // en lugar de implementar varias veriones de lógica para cada caso aquí.
    fun validateToOpen(user: User) {
        validationStrategy.validateUser(user)
    }
}